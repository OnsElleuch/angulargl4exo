import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RainbowComponent } from './rainbow/rainbow.component';
import { RainbowDirective } from './rainbow/rainbow.directive';

@NgModule({
  declarations: [
    AppComponent,
    RainbowComponent,
    RainbowDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
