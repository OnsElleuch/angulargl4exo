import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RainbowComponent } from './rainbow/rainbow.component';

const routes: Routes = [
  {
    path: "rainbow",
    component : RainbowComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
